// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair
#include <set>   // set

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
    graph_types =
    Types<
        boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
        Graph // uncomment
            >;

#ifdef __APPLE__
    TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
    TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

// add vertex to empty graph
TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);}


// add edge between to existing vertices
TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);}

// vertex_iterator test, non-empty graph
TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    set<vertex_descriptor> ex;
    ex.insert(vdA);
    ex.insert(vdB);
    set<vertex_descriptor> r;
    r.insert(*b);
    ++b;
    ASSERT_NE(b, e);

    r.insert(*b);
    ++b;
    ASSERT_EQ(b, e);
    ASSERT_EQ(ex,r);}

// edge_iterator test, non-empty graph
TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    set<edge_descriptor> ex;
    ex.insert(edAB);
    ex.insert(edAC);
    set<edge_descriptor> r;

    edge_descriptor ed1 = *b;
    r.insert(ed1);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    r.insert(ed2);
    ++b;
    ASSERT_EQ(e, b);
    ASSERT_EQ(ex, r);}

// adjacency_iterator test, have adjacent vertex
TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    set<vertex_descriptor> ex;
    ex.insert(vdC);
    ex.insert(vdB);
    set<vertex_descriptor> r;
 
    r.insert(*b);
    ++b;
    ASSERT_NE(b, e);

    r.insert(*b);
    ++b;
    ASSERT_EQ(e, b);
    ASSERT_EQ(ex, r);}

// adjacency_iterator test, have adjacent vertex
TYPED_TEST(GraphFixture, test5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);


    add_edge(vdA, vdC, g);
    add_edge(vdB, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);
    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdC);
    ++b;
    ASSERT_EQ(b, e);

    p = adjacent_vertices(vdB, g);
    b = p.first;
    e = p.second;
    ASSERT_NE(b, e);
    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ASSERT_EQ(vd2, vd1);
    ++b;
    ASSERT_EQ(b, e);

    p = adjacent_vertices(vdC, g);
    b = p.first;
    e = p.second;
    ASSERT_EQ(b, e);}


// add edge between to vertices not in the graph, but the vertex_descriptor exit
// add an edge between a vertex ane itself
TYPED_TEST(GraphFixture, test6) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g2);

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g1);

    ASSERT_EQ(p1.second, true);

    vertex_descriptor vd1 = source(p1.first, g1);
    ASSERT_EQ(vd1, vdA);
    vertex_descriptor vd2 = target(p1.first, g1);
    ASSERT_EQ(vd2, vdB);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g1);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd = *b;
    ASSERT_EQ(vd, vdA);
    ++b;
    ASSERT_EQ(b, e);

    ASSERT_EQ(num_edges(g1), 1);
    ASSERT_EQ(num_vertices(g1), 1);
    ASSERT_EQ(num_edges(g2), 0);
    ASSERT_EQ(num_vertices(g2), 1);
}

// add edge between to vertices not in the graph, auto populate
TYPED_TEST(GraphFixture, test7) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;

    for(int i = 0; i < 5; i++){
        add_vertex(g1);
    }
    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g2);
    pair<edge_descriptor, bool> p = add_edge(vdA, vdB, g2);
    ASSERT_EQ(p.second, true);

    vertex_descriptor vd3 = source(p.first, g2);
    ASSERT_EQ(vd3, vdA);
    vertex_descriptor vd4 = target(p.first, g2);
    ASSERT_EQ(vd4, vdB);

    ASSERT_EQ(num_edges(g1), 0);
    ASSERT_EQ(num_vertices(g1), 6);
    
    ASSERT_EQ(num_edges(g2), 1);
    ASSERT_EQ(num_vertices(g2), 6);
}

// add edge between to vertices not in the graph, auto populate
TYPED_TEST(GraphFixture, test8) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;
    
    vertex_descriptor vdA = add_vertex(g2);
    vertex_descriptor vdB = add_vertex(g2);
    pair<edge_descriptor, bool> p = add_edge(vdA, vdB, g1);
    ASSERT_EQ(p.second, true);

    vertex_descriptor vd1 = source(p.first, g1);
    ASSERT_EQ(vd1, vdA);
    vertex_descriptor vd2 = target(p.first, g1);
    ASSERT_EQ(vd2, vdB);

    ASSERT_EQ(num_edges(g1), 1);
    ASSERT_EQ(num_vertices(g1), 2);
    
    ASSERT_EQ(num_edges(g2), 0);
    ASSERT_EQ(num_vertices(g2), 2);
}

// adjacency_iterator test, have adjacent vertex
TYPED_TEST(GraphFixture, test9) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdB, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_EQ(e, b);

    p = adjacent_vertices(vdC, g);
    b = p.first;
    e = p.second;
    ASSERT_EQ(e, b);}

// adjacency_iterator test, have adjacent vertex
TYPED_TEST(GraphFixture, test10) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdA, g);
    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdB, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(e, b);
    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_EQ(e, b);

    p = adjacent_vertices(vdA, g);
    b = p.first;
    e = p.second;
    ASSERT_NE(e, b);

    set<vertex_descriptor> ex ({0,1});
    set<vertex_descriptor> r;
    r.insert(*b);
    ++b;
    r.insert(*b);
    ++b;

    ASSERT_EQ(ex, r);
    ASSERT_EQ(e, b);}

// retrieve an edge 
TYPED_TEST(GraphFixture, test11) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;
    
    vertex_descriptor vdA = add_vertex(g2);
    vertex_descriptor vdB = add_vertex(g2);
    pair<edge_descriptor, bool> p = add_edge(vdA, vdB, g1);
    ASSERT_EQ(p.second, true);

    pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g1);
    ASSERT_EQ(e1.second, true);

    pair<edge_descriptor, bool> e2 = edge(vdB, vdA, g1);
    ASSERT_EQ(e2.second, false);
}

// retrieve an edge
TYPED_TEST(GraphFixture, test12) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;
    
    vertex_descriptor vdA = add_vertex(g2);
    vertex_descriptor vdB = add_vertex(g2);
    vertex_descriptor vdC = add_vertex(g1);

    pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g2);
    ASSERT_EQ(e1.second, false);
    e1 = edge(vdB, vdA, g2);
    ASSERT_EQ(e1.second, false);

    pair<edge_descriptor, bool> e2 = edge(vdC, vdC, g1);
    ASSERT_EQ(e2.second, false);

    ASSERT_EQ(num_edges(g1), 0);
    ASSERT_EQ(num_edges(g2), 0);
}

//  num_edges
TYPED_TEST(GraphFixture, test13) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    
    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g1);
    pair<edge_descriptor, bool> p = add_edge(vdA, vdB, g1);
    ASSERT_EQ(p.second, true);

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g1);
    ASSERT_EQ(p1.second, false);

    ASSERT_EQ(num_edges(g1), 1);
}

//  num_edges
TYPED_TEST(GraphFixture, test14) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    
    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g1);
    vertex_descriptor vdC = add_vertex(g1);
    vertex_descriptor vdD = add_vertex(g1);

    pair<edge_descriptor, bool> p = add_edge(vdA, vdB, g1);
    ASSERT_EQ(p.second, true);

    pair<edge_descriptor, bool> p1 = add_edge(vdB, vdA, g1);
    ASSERT_EQ(p1.second, true);

    pair<edge_descriptor, bool> p2 = add_edge(vdB, vdD, g1);
    ASSERT_EQ(p2.second, true);

    pair<edge_descriptor, bool> p3 = add_edge(vdD, vdC, g1);
    ASSERT_EQ(p3.second, true);

    pair<edge_descriptor, bool> p4 = add_edge(vdA, vdB, g1);
    ASSERT_EQ(p4.second, false);

    ASSERT_EQ(num_edges(g1), 4);
}


//  num_edges, num_vertices
TYPED_TEST(GraphFixture, test15) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g1;

    ASSERT_EQ(num_edges(g1), 0);
    ASSERT_EQ(num_vertices(g1), 0);
}

//  source, target
TYPED_TEST(GraphFixture, test16) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    
    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g1);
    pair<edge_descriptor, bool> p = add_edge(vdB, vdA, g1);
    ASSERT_EQ(p.second, true);

    pair<edge_descriptor, bool> edAB = edge(vdA, vdB, g1);
    ASSERT_EQ(edAB.second, false);

    vertex_descriptor vd1 = source(edAB.first, g1);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB.first, g1);
    ASSERT_EQ(vd2, vdB);

    ASSERT_EQ(num_edges(g1), 1);
}

//  source, target
TYPED_TEST(GraphFixture, test17) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    
    vertex_descriptor vdA = add_vertex(g1);
    pair<edge_descriptor, bool> p = add_edge(vdA, vdA, g1);
    ASSERT_EQ(p.second, true);


    vertex_descriptor vd1 = source(p.first, g1);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(p.first, g1);
    ASSERT_EQ(vd2, vdA);

    ASSERT_EQ(num_edges(g1), 1);
}

// edge, destination v not in the graph
TYPED_TEST(GraphFixture, test18) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g1);
    add_vertex(g2);
    vertex_descriptor vdC = add_vertex(g2);
    add_vertex(g2);
    pair<edge_descriptor, bool> ed = edge(vdA, vdC, g1);
    ASSERT_EQ(ed.second, false);
    ASSERT_EQ(1, num_vertices(g1));
    ASSERT_EQ(3, num_vertices(g2));
}

//  source, target
TYPED_TEST(GraphFixture, test19) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    
    pair<edge_descriptor, bool> p = add_edge(0, 0, g1);
    ASSERT_EQ(p.second, true);

    vertex_descriptor vd1 = source(p.first, g1);
    ASSERT_EQ(vd1, 0);

    vertex_descriptor vd2 = target(p.first, g1);
    ASSERT_EQ(vd2, 0);

    ASSERT_EQ(num_edges(g1), 1);
    ASSERT_EQ(num_vertices(g1), 1);
}

//  vertex
TYPED_TEST(GraphFixture, test20) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g1);
    add_vertex(g2);
    vertex_descriptor vdB = add_vertex(g2);
    vertex_descriptor vdC = add_vertex(g2);

    add_edge(vdB, vdC, g1);
    vertex_descriptor vd = vertex(3, g2);
    ASSERT_EQ(vd, 3);

    vertex_descriptor vd1 = vertex(0, g1);
    ASSERT_EQ(vd1, vdA);
}

//  vertex
TYPED_TEST(GraphFixture, test21) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g1;

    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g1);
    vertex_descriptor vdC = add_vertex(g1);

    vertex_descriptor vd = vertex(0, g1);
    ASSERT_EQ(vdA, vd);
    vertex_descriptor vd1 = vertex(1, g1);
    ASSERT_EQ(vdB, vd1);
    vertex_descriptor vd2 = vertex(2, g1);
    ASSERT_EQ(vdC, vd2);

    ASSERT_EQ(num_vertices(g1), 3);
}

// add_vertex
TYPED_TEST(GraphFixture, test22) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    for (int i = 0; i < 100; ++i) {
        vertex_descriptor vdA = add_vertex(g);
        ASSERT_EQ(vdA, i);
    }
}

// add_vertex
TYPED_TEST(GraphFixture, test23) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    for (int i = 0; i < 100; ++i) {
        vertex_descriptor vdA = add_vertex(g);
        ASSERT_EQ(vdA, i);
    }
    ASSERT_EQ(num_vertices(g), 100);
}

// add_vertex
TYPED_TEST(GraphFixture, test24) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    for (int i = 0; i < 100; ++i) {
        vertex_descriptor vdA = add_vertex(g);
        add_edge(vdA, i/2, g);
        ASSERT_EQ(vdA, i);
    }
    ASSERT_EQ(num_vertices(g), 100);
    ASSERT_EQ(num_edges(g), 100);
}

// vertices iterator on empty graph
TYPED_TEST(GraphFixture, test25) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_iterator    = typename TestFixture::vertex_iterator;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_EQ(b, e);

    pair<vertex_iterator, vertex_iterator> p1 = vertices(g);
    vertex_iterator                        b1 = p1.first;
    vertex_iterator                        e1 = p1.second;
    ASSERT_EQ(b1, e1);
}

//  adj vertices
TYPED_TEST(GraphFixture, test26) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g1;
    
    vertex_descriptor vdA = add_vertex(g1);
    pair<edge_descriptor, bool> p = add_edge(vdA, vdA, g1);
    ASSERT_EQ(p.second, true);

    pair<adjacency_iterator, adjacency_iterator> it = adjacent_vertices(vdA, g1);
    adjacency_iterator                           b = it.first;
    adjacency_iterator                           e = it.second;
    ASSERT_NE(b, e);

    ASSERT_EQ(*b, vdA);
    ++b;
    ASSERT_EQ(b, e);

    ASSERT_EQ(num_edges(g1), 1);
}

// edge iterator
TYPED_TEST(GraphFixture, test27) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_descriptor, bool> ed = add_edge(vdA, vdB, g);
    ASSERT_EQ(ed.second, true);
    pair<edge_descriptor, bool> ed2 = add_edge(vdB, vdA, g);
    ASSERT_EQ(ed2.second, true);
    pair<edge_descriptor, bool> ed3 = add_edge(vdB, vdC, g);
    ASSERT_EQ(ed3.second, true);

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);
    ++++++b;
    ASSERT_EQ(b, e);
}

// source
TYPED_TEST(GraphFixture, test28) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vd = add_vertex(g);
    for (int i = 1; i < 100; ++i) {
        vertex_descriptor vdA = add_vertex(g);
        pair<edge_descriptor, bool> ed = add_edge(vdA, vd, g);
        ASSERT_EQ(source(ed.first, g), vdA);
    }
}

// adj vertices
TYPED_TEST(GraphFixture, test29) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vd = add_vertex(g);
    for (int i = 1; i < 100; ++i) {
        vertex_descriptor vdA = add_vertex(g);
        add_edge(vd, vdA, g);
    }

    pair<adjacency_iterator, adjacency_iterator> it = adjacent_vertices(vd, g);
    adjacency_iterator                           b = it.first;
    adjacency_iterator                           e = it.second;
    ASSERT_NE(b, e);

    int count = 0;
    while(b!= e){
        ++b;
        ++count;
    }

    ASSERT_EQ(count, 99);
}

// adj vertices
TYPED_TEST(GraphFixture, test30) {
        using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vd = add_vertex(g);
    for (int i = 1; i < 100; ++i) {
        vertex_descriptor vdA = add_vertex(g);
        add_edge(vdA, vd, g);
    }

    pair<adjacency_iterator, adjacency_iterator> it = adjacent_vertices(vd, g);
    adjacency_iterator                           b = it.first;
    adjacency_iterator                           e = it.second;
    ASSERT_EQ(b, e);    
}
