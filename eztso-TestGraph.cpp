// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair
#include "Graph.hpp"
#include <cstdlib>

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"


// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
//boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>
using
graph_types =
    Types<boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>, Graph>;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;
    add_vertex(g);
    add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);


    //ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);
    //ASSERT_EQ(vd2, vdB);

    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_iterator   = typename TestFixture::vertex_iterator;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_EQ(b, e);
    pair<edge_iterator, edge_iterator> pp = edges(g);
    edge_iterator                        bb = pp.first;
    edge_iterator                        ee = pp.second;
    ASSERT_EQ(bb, ee);


}

TYPED_TEST(GraphFixture, test4) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_iterator   = typename TestFixture::vertex_iterator;
    graph_type g;
    int N = 20;
    for(int i = 0; i < N; i++) {
        add_vertex(g);
    }

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_EQ(std::distance(b, e), 20);


}

TYPED_TEST(GraphFixture, test5) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_iterator   = typename TestFixture::edge_iterator;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;
    const int N = 20;
    vertex_descriptor v[N];
    std::unordered_map<int, int> m;
    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        m[v[i]] = i;
    }
    for(int i = 0; i < N - 1; i++) {
        add_edge(v[i], v[i+1], g);
    }

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                        b = p.first;
    edge_iterator                        e = p.second;
    ASSERT_EQ(std::distance(b, e), 19);
    while(b!=e) {
        auto edge = *b;
        ASSERT_EQ(m[source(edge, g)], m[target(edge, g)] - 1);
        ++b;
    }
}

TYPED_TEST(GraphFixture, test6) {
    using graph_type        = typename TestFixture::graph_type;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;
    const int N = 20;
    vertex_descriptor v[N];
    std::unordered_map<int, int> m;
    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        m[v[i]] = i;
    }
    for(int i = 0; i < N - 1; i++) {
        add_edge(v[i], v[i+1], g);
    }
    for (int i = 0; i < N-1; i++) {
        auto vert = v[i];
        pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vert, g);
        adjacency_iterator                        b = p.first;
        adjacency_iterator                        e = p.second;
        ASSERT_EQ(std::distance(b, e), 1);
        while(b!=e) {
            auto dest = *b;
            ASSERT_EQ(m[vert], m[dest] - 1);
            ++b;
        }
    }
}
TYPED_TEST(GraphFixture, test7) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;
    const int N = 20;
    vertex_descriptor v[N];
    std::unordered_map<int, int> m;
    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        m[v[i]] = i;
    }
    for(int i = 0; i < N - 1; i++) {
        add_edge(v[i], v[i+1], g);
        ASSERT_EQ(edge(v[i],v[i+1], g).second, true);
        ASSERT_EQ(add_edge(v[i],v[i+1], g).second, false);

    }
    ASSERT_EQ(edge(v[N-1],v[0], g).second, false);
    ASSERT_EQ(add_edge(v[N-1],v[0], g).second, true);
}

TYPED_TEST(GraphFixture, test8) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;
    const int N = 20;
    vertex_descriptor v[N];
    std::unordered_map<int, int> m;
    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        m[v[i]] = i;
    }
    for(int i = 0; i < N - 1; i++) {
        add_edge(v[i], v[i+1], g);
    }

    ASSERT_EQ(num_vertices(g), N);
}


TYPED_TEST(GraphFixture, test9) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;
    const int N = 20;
    vertex_descriptor v[N];
    std::unordered_map<int, int> m;
    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        m[v[i]] = i;
    }
    for(int i = 0; i < N - 1; i++) {
        add_edge(v[i], v[i+1], g);
    }

    ASSERT_EQ(num_edges(g), N-1);
}

TYPED_TEST(GraphFixture, test10) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;
    const int N = 20;
    vertex_descriptor v[N];
    std::unordered_map<int, int> m;
    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        m[v[i]] = i;
    }
    for(int i = 0; i < N - 1; i++) {
        auto res = edge(v[i], v[i+1], g).first;

        ASSERT_EQ(source(res, g), v[i]);
        ASSERT_EQ(target(res, g), v[i+1]);


    }
}

TYPED_TEST(GraphFixture, test11) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;
    const int N = 20;
    vertex_descriptor v[N];
    std::unordered_map<int, int> m;
    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        m[v[i]] = i;
    }
    for(int i = 0; i < N - 1; i++) {
        auto res = edge(v[i], v[i+1], g).first;

        ASSERT_EQ(source(res, g), v[i]);
        ASSERT_EQ(target(res, g), v[i+1]);


    }
}


TYPED_TEST(GraphFixture, test12) {
    using graph_type        = typename TestFixture::graph_type;
    graph_type g;
    ASSERT_EQ(num_vertices(g), 0);
}
TYPED_TEST(GraphFixture, test13) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;
    vertex_descriptor x = add_vertex(g);
    vertex_descriptor y = add_vertex(g);
    ASSERT_EQ(edge(x, y, g).second, false);
}
TYPED_TEST(GraphFixture, test14) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_iterator = typename TestFixture::edge_iterator;
    graph_type g;
    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                        b = p.first;
    edge_iterator                        e = p.second;

    ASSERT_EQ(std::distance(b, e), 0);
}

TYPED_TEST(GraphFixture, test15) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_iterator = typename TestFixture::vertex_iterator;
    graph_type g;
    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;

    ASSERT_EQ(std::distance(b, e), 0);
}

TYPED_TEST(GraphFixture, test16) {
    using graph_type        = typename TestFixture::graph_type;
    graph_type g;
    ASSERT_EQ(num_edges(g), 0);
}
TYPED_TEST(GraphFixture, test17) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_iterator = typename TestFixture::vertex_iterator;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;
    graph_type g;
    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    while(b!=e) {
        pair<adjacency_iterator, adjacency_iterator> x = adjacent_vertices(*b, g);
        adjacency_iterator x1 = x.first;
        adjacency_iterator x2 = x.second;
        ASSERT_EQ(std::distance(x1, x2), 0);
        ASSERT_EQ(x1, x2);
        ++b;
    }
}

TYPED_TEST(GraphFixture, test18) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;
    graph_type g;

    const int N = 200;
    vertex_descriptor v[N];
    std::unordered_map<int, int> m;
    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        m[v[i]] = i;
    }
    for (vertex_descriptor ve1: v) {
        for (vertex_descriptor ve2: v) {

            add_edge(ve1, ve2, g);

        }
    }

    ASSERT_EQ(num_edges(g), N * N);

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                        b = p.first;
    edge_iterator                        e = p.second;

    ASSERT_EQ(std::distance(b, e), N * N);
}

TYPED_TEST(GraphFixture, test19) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;
    graph_type g;

    const int N = 700;
    vertex_descriptor v[N];
    std::unordered_map<int, int> m;
    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        m[v[i]] = i;
    }
    for (vertex_descriptor ve1: v) {
        for (vertex_descriptor ve2: v) {

            add_edge(ve1, ve2, g);

        }
    }


    ASSERT_EQ(num_edges(g), N * N);

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                        b = p.first;
    edge_iterator                        e = p.second;

    ASSERT_EQ(std::distance(b, e), N * N);
}


TYPED_TEST(GraphFixture, test20) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;

    const int N = 100;
    vertex_descriptor v[N];
    // std::unordered_map<int, int> m;

    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        // m[v[i]] = i;
    }
    for (vertex_descriptor ve1: v) {
        for (vertex_descriptor ve2: v) {

            add_edge(ve1, ve2, g);

        }
    }
    int i1 =std::rand() % N;
    int i2 =std::rand() % N;
    ASSERT_EQ(add_edge(i1, i2, g).second, false);
}


TYPED_TEST(GraphFixture, test21) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator = typename TestFixture::vertex_iterator;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;


    graph_type g;

    const int N = 100;
    vertex_descriptor v[N];
    // std::unordered_map<int, int> m;

    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        // m[v[i]] = i;
    }
    for (vertex_descriptor ve1: v) {
        for (vertex_descriptor ve2: v) {

            add_edge(ve1, ve2, g);

        }
    }
    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_EQ(std::distance(b, e), N);
    std::unordered_set<vertex_descriptor> adj;
    while(b!=e) {
        for(int i = 0; i < N; i++) {
            adj.insert(v[i]);
        }
        ASSERT_EQ(adj.size(), N);

        pair<adjacency_iterator, adjacency_iterator> pp = adjacent_vertices(*b, g);

        adjacency_iterator                        bb = pp.first;
        adjacency_iterator                        ee = pp.second;
        while(bb != ee) {
            adj.erase(*bb);
            ++bb;
        }
        ASSERT_EQ(adj.size(), 0);

        ++b;
    }
}
TYPED_TEST(GraphFixture, test22) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator = typename TestFixture::vertex_iterator;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;


    graph_type g;

    const int N = 100;
    vertex_descriptor v[N];
    // std::unordered_map<int, int> m;

    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        // m[v[i]] = i;
    }
    int r1 = std::rand() % N;
    for (vertex_descriptor ve1: v) {
        for (int i = 0; i < r1; ++i) {
            add_edge(ve1, v[i], g);
        }
    }
    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_EQ(std::distance(b, e), N);
    while(b!=e) {
        pair<adjacency_iterator, adjacency_iterator> pp = adjacent_vertices(*b, g);

        adjacency_iterator                        bb = pp.first;
        adjacency_iterator                        ee = pp.second;
        ASSERT_EQ(std::distance(bb, ee), r1);


        ++b;
    }
}

TYPED_TEST(GraphFixture, test23) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator = typename TestFixture::vertex_iterator;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;


    graph_type g;

    const int N = 500;
    vertex_descriptor v[N];
    // std::unordered_map<int, int> m;

    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        // m[v[i]] = i;
    }
    int r1 = std::rand() % N;
    for (vertex_descriptor ve1: v) {
        for (int i = 0; i < r1; ++i) {
            add_edge(ve1, v[i], g);
        }
    }
    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_EQ(std::distance(b, e), N);
    while(b!=e) {
        pair<adjacency_iterator, adjacency_iterator> pp = adjacent_vertices(*b, g);

        adjacency_iterator                        bb = pp.first;
        adjacency_iterator                        ee = pp.second;
        ASSERT_EQ(std::distance(bb, ee), r1);

        ++b;
    }
}

TYPED_TEST(GraphFixture, test24) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator = typename TestFixture::vertex_iterator;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;


    graph_type g;

    const int N = 100;
    vertex_descriptor v[N];
    // std::unordered_map<int, int> m;

    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        // m[v[i]] = i;
    }
    int r2 = std::rand() % N;
    for (vertex_descriptor ve1: v) {
        add_edge(ve1, v[r2], g);

    }
    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_EQ(std::distance(b, e), N);
    while(b!=e) {
        pair<adjacency_iterator, adjacency_iterator> pp = adjacent_vertices(*b, g);

        adjacency_iterator                        bb = pp.first;
        adjacency_iterator                        ee = pp.second;
        ASSERT_EQ(std::distance(bb, ee), 1);
        ASSERT_EQ(*bb, v[r2]);

        ++b;
    }
    ASSERT_EQ(num_edges(g), num_vertices(g));
}
TYPED_TEST(GraphFixture, test25) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator = typename TestFixture::vertex_iterator;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;


    graph_type g;

    const int N = 500;
    vertex_descriptor v[N];
    // std::unordered_map<int, int> m;

    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        // m[v[i]] = i;
    }
    int r2 = std::rand() % N;
    for (vertex_descriptor ve1: v) {
        add_edge(ve1, v[r2], g);

    }
    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_EQ(std::distance(b, e), N);
    while(b!=e) {
        pair<adjacency_iterator, adjacency_iterator> pp = adjacent_vertices(*b, g);

        adjacency_iterator                        bb = pp.first;
        adjacency_iterator                        ee = pp.second;
        ASSERT_EQ(std::distance(bb, ee), 1);
        ASSERT_EQ(*bb, v[r2]);

        ++b;
    }
    ASSERT_EQ(num_edges(g), num_vertices(g));
}

TYPED_TEST(GraphFixture, test26) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator = typename TestFixture::vertex_iterator;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;


    graph_type g;

    const int N = 100;
    vertex_descriptor v[N];
    // std::unordered_map<int, int> m;

    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        // m[v[i]] = i;
    }
    for (vertex_descriptor ve1: v) {
        for (vertex_descriptor ve2: v) {

            add_edge(ve1, ve2, g);

        }
    }
    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    int i = 0;
    ASSERT_EQ(std::distance(b, e), N);
    std::unordered_set<vertex_descriptor> adj;
    while(b!=e) {
        for(int i = 0; i < N; i++) {
            adj.insert(v[i]);
        }
        ASSERT_EQ(adj.size(), N);

        pair<adjacency_iterator, adjacency_iterator> pp = adjacent_vertices(*b, g);

        adjacency_iterator                        bb = pp.first;
        adjacency_iterator                        ee = pp.second;
        while(bb != ee) {
            ++i;
            ++bb;
        }

        ++b;
    }
    ASSERT_EQ(i, N * N);
}

TYPED_TEST(GraphFixture, test27) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator = typename TestFixture::vertex_iterator;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;


    graph_type g;

    const int N = 1000;
    vertex_descriptor v[N];
    // std::unordered_map<int, int> m;

    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        // m[v[i]] = i;
    }
    for (vertex_descriptor ve1: v) {
        for (vertex_descriptor ve2: v) {

            add_edge(ve1, ve2, g);

        }
    }
    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    int i = 0;
    ASSERT_EQ(std::distance(b, e), N);
    std::unordered_set<vertex_descriptor> adj;
    while(b!=e) {
        for(int i = 0; i < N; i++) {
            adj.insert(v[i]);
        }
        ASSERT_EQ(adj.size(), N);

        pair<adjacency_iterator, adjacency_iterator> pp = adjacent_vertices(*b, g);

        adjacency_iterator                        bb = pp.first;
        adjacency_iterator                        ee = pp.second;
        while(bb != ee) {
            ++i;
            ++bb;
        }

        ++b;
    }
    ASSERT_EQ(i, N * N);
}


TYPED_TEST(GraphFixture, test28) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;


    graph_type g;

    const int N = 1000;
    vertex_descriptor v[N];
    // std::unordered_map<int, int> m;

    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        // m[v[i]] = i;
    }
    for(int i = 0; i < N; i++) {
        ASSERT_EQ(v[i], vertex(i, g));
    }

}


TYPED_TEST(GraphFixture, test29) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);


    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 2);

    ASSERT_EQ(add_edge(vdA, vdB, g).second, true);
    ASSERT_EQ(add_edge(vdB, vdA, g).second, true);
}

TYPED_TEST(GraphFixture, test30) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);


    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 2);

    ASSERT_EQ(add_edge(vdA, vdB, g).second, true);
    ASSERT_EQ(add_edge(vdB, vdA, g).second, true);

    ASSERT_EQ(add_edge(vdA, vdB, g).second, false);
    ASSERT_EQ(add_edge(vdB, vdA, g).second, false);
}

TYPED_TEST(GraphFixture, test31) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;

    const int N = 100;
    vertex_descriptor v[N];
    // std::unordered_map<int, int> m;

    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        // m[v[i]] = i;
    }
    for (vertex_descriptor ve1: v) {
        for (vertex_descriptor ve2: v) {

            add_edge(ve1, ve2, g);

        }
    }

    for (vertex_descriptor ve1: v) {
        for (vertex_descriptor ve2: v) {

            ASSERT_EQ(edge(ve1, ve2, g).second, true);

        }
    }
}

TYPED_TEST(GraphFixture, test32) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;

    const int N = 1000;
    vertex_descriptor v[N];
    // std::unordered_map<int, int> m;

    for(int i = 0; i < N; i++) {
        v[i] = add_vertex(g);
        // m[v[i]] = i;
    }
    for (vertex_descriptor ve1: v) {
        for (vertex_descriptor ve2: v) {

            add_edge(ve1, ve2, g);

        }
    }

    for (vertex_descriptor ve1: v) {
        for (vertex_descriptor ve2: v) {

            ASSERT_EQ(edge(ve1, ve2, g).second, true);

        }
    }
}
